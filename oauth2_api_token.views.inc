<?php
/**
 * @file
 * Views integration for the OAuth2 API Token module.
 */

/**
 * Implements hook_views_data_alter().
 */
function oauth2_api_token_views_data_alter(&$data) {
  if (isset($data['oauth2_server_token'])) {
    $data['oauth2_server_token']['manage_links'] = [
      'title' => t('Links'),
      'help' => t('Links to manage the API token.'),
      'field' => [
        'handler' => 'OAuth2ApiTokenViewsHandlerFieldLinks',
        'type' => 'oauth2_api_token',
        'real field' => 'token_id',
      ],
    ];
  }
}
