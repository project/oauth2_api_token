<?php

class OAuth2ApiTokenViewsHandlerFieldLinks extends views_handler_field {
  public function render($values) {
    if (!isset($values->token_id)) {
      return '';
    }

    $token = oauth2_api_token_load($values->token_id);
    if ($token) {
      $links = [];
      $path = 'user/' . $token->uid . '/api-tokens/' . $values->token_id;
      if (oauth2_api_token_access('view', $token)) {
        $links[] = l(t('View'), $path, ['html' => TRUE]);
      }
      if (oauth2_api_token_access('update', $token)) {
        $links[] = l(t('Edit'), $path . '/edit', ['html' => TRUE]);
      }
      if (oauth2_api_token_access('delete', $token)) {
        $links[] = l(t('Delete'), $path . '/delete', ['html' => TRUE]);
      }

      return implode(' &middot; ', $links);
    }

    return '';
  }
}
